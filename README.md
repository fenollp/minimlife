#minimlife • [Bitbucket](https://bitbucket.org/fenollp/minimlife)

## A minimilistic game of life

### How To Use:
* Compile and run in the Erlang shell: `make && erl -pa ebin/`
* Step-by-step game:

```erlang
life:game(100). %% Spawns a game table and its 100*100 cells.
life:print().   %% Display initial table filing.
life:store("game.terms").  %% Store current game to file.
life:next().
life:next().
life:next().
life:print().   %% Play Life a few times & display table.
life:load("game.terms").
life:print().   %% Load an old session & show it.
life:stop().    %% Un-register global pid 'table'.
```

* Auto game:

```erlang
tp1:game(100).  %% A table of 10000 cells. (using a process' dictionary)
tp1:load("tarter.terms").
tp1:play(100). %% Step & print every 100 milliseconds.
%% I suggest using a very small typeface while running this.
```

* End|Close shell with `^G q` or `^C^C^C`
