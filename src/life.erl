%% Copyright © 2014 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(life).

%% life: a simple, minimalistic implementation of Conway's Game of Life.

-export([ game/1  %% Starts the table-process ==> needs to be called first.
        , store/1
        , load/1, load/2
        , next/0
        , print/0
        , stop/0
        , play/1 ]).


-ifdef(debug).
- define(f(L), io:format(user, "== ~p\n", [L]),).
-else.
- define(f(_), ).
-endif.


%% API

game (M)
  when is_integer(M), M >= 10 ->
    Pid = spawn(fun () -> table(M) end),
    Pid ! register,
    ok.

store (Filename) ->
    table ! {store, Filename},
    ok.

%% We store table's size (M) & contents (T) as Erlang terms to a text file.
%%   Thus we have file:consult/1 for easy retrieval
%%     and element(X, T) for O(1) access to the contents to be loaded.
load (M, State)
  when is_tuple(State) ->
    F = fun (I) ->
                table ! {set, I, lifestr(element(I,State))}
        end,
    impure_iter(F, M * M).
load (Filename)
  when is_list(Filename) ->
    {ok, Terms} = file:consult(Filename),
    {_, M} = lists:keyfind(m, 1, Terms),
    {_, T} = lists:keyfind(t, 1, Terms),
    load(M, T).

%% Send will badarg if ‘table’ is not registered yet.
next  () -> table ! step.
print () -> table ! print.
stop  () -> table ! stop.

play (Interval) ->
    print(),
    next(),
    timer:sleep(Interval),
    play(Interval).

%% Internals

%% A simple iterator so as to
%%  - not have to create too many recursive functions (verbose)
%%  - not have to use lists:seq/2 (memory-greedy)
impure_iter (_, 0) -> ok;
impure_iter (F, I) ->
    F(I),
    impure_iter(F, I - 1).


%% At each new step we create a new table-holding process
%%   and fill it using the previous one.
%% The table-process uses its process dictionary for speed.
%% A cell's state is a boolean(). As time goes by more and more cells die
%%   thus we do not store dead cells.
table (M) ->
    F = fun (I) ->
                case random:uniform() < 0.5 of
                    false -> ok;
                    true  -> put(I, true)
                end
        end,
    impure_iter(F, M * M),
    game_loop(M).

game_loop (M) ->
    receive
        register ->
            true = register(table, self()),
            game_loop(M);
        step ->
            NewTable = spawn(fun () -> table(M) end),
            set_next_table(M, NewTable),
            true = unregister(table),
            NewTable ! register;
        {set, I, Life} ->
            put(I, Life),%
            game_loop(M);
        print ->
            print_table(M),
            game_loop(M);
        {store, Filename} ->
            IOData = [ "{m, ", integer_to_list(M), "}.\n"
                     , "{t, { % Write a space or a # in the places you want.\n"
                     ,     store_to_iolist(M)
                     , "\n}}.\n" ],
            ok = file:write_file(Filename, IOData),
            game_loop(M);
        stop ->
            case whereis(table) of
                Pid when Pid =:= self() -> unregister(table);
                _ -> ok
            end,
            ok;
        _M ->
            ?f([self(),un_received,_M])
            game_loop(M)
    end.

set_next_table (M, Pid) ->
    F = fun (I) ->
                SumF = fun (N, Acc) ->
                               case 'alive?'(N) of
                                   true  -> Acc + 1;
                                   false -> Acc + 0
                               end
                       end,
                CountAliveN = lists:foldl(SumF, 0, neighbours(I,M)),
                CellNextState = 'alive?'('alive?'(I), CountAliveN),
                %set_life(Pid, I, CellNextState) % FIX
                Pid ! {set, I, CellNextState}    % MEE
                %% If set_life/3 is used here, the game just blinks
                %%   (oscillates between a few states)…
        end,
    impure_iter(F, M * M).

%% Does not send dead cells: saves memory each step.
set_life (Pid, Cell, Liveliness) ->
    case Liveliness of
        false -> ok;
        true  -> Pid ! {set, Cell, true}
    end.

print_table (M) ->
    TupleStr = table2tupleStr(M),
    print_line(M),
    split_print(M, TupleStr),
    print_line(M).

split_print (_, []) -> ok;
split_print (M, TupleStr) ->
    {Line, Rest} = lists:split(M, TupleStr),
    Str = lists:flatten(Line),
    io:format("|~s|\n", [Str]),
    split_print(M, Rest).

split_insert (_, []) -> [];
split_insert (M, TupleStr) ->
    {Line, Rest} = lists:split(M, TupleStr),
    [string:join(Line, ",") | split_insert(M, Rest)].

store_to_iolist (M) ->
    TupleStr = table2tupleStr(M),
    Lines = split_insert(M, TupleStr),
    insert(",\n", Lines).


table2tupleStr (M) ->
    table2tupleStr(M * M, M, []).
table2tupleStr (0, _, Acc) -> Acc;
table2tupleStr (I, M, Acc) ->
    Char = lifestr('alive?'(I)),
    table2tupleStr(I -1, M, [Char|Acc]).

lifestr (true)  -> "'#'";
lifestr (false) -> "' '";
lifestr ('#') -> true;
lifestr (' ') -> false.

insert (_, []) -> [];
insert (_, [H]) -> [H];
insert (Sep, [H|T]) ->
    [H, Sep | insert(Sep,T)].

print_line (M) ->
    Line = string:copies("===", M),
    io:format("~s\n", [Line]).

neighbours (I, M) -> %%FIXME to not convert twice
    X = case I rem M of
            0 -> M;
            Rx -> Rx
        end,
    Y = case {I div M, I rem M} of
            {Dy,0} -> Dy;
            {Ry,_} -> Ry +1
        end,
    L = case X of 1 -> M; _ -> X -1 end, %% Left
    R = case X of M -> 1; _ -> X +1 end, %% Right
    U = case Y of 1 -> M; _ -> Y -1 end, %% Up
    D = case Y of M -> 1; _ -> Y +1 end, %% Down
    [ M * (Yy -1) + Xx
      || {Xx,Yy} <- [ {L,U}, {X,U}, {R,U}
                    , {L,Y},        {R,Y}
                    , {L,D}, {X,D}, {R,D} ] ].

'alive?' (Cell) when is_integer(Cell) ->
    case get(Cell) of
        undefined -> false;
        Other     -> Other
    end.

'alive?' (true,  Neighbours) when Neighbours /= 2
                                  andalso Neighbours /= 3 -> false;
'alive?' (false, Neighbours) when Neighbours == 3 -> true;
'alive?' (State, _) -> State.

%% End of Module.
